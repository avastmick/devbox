#!/usr/bin/env bash
set -e

if [ $(whoami) = "root" ]; then
	exec sudo -u vagrant -i /vagrant/bootstrap.sh
fi

mkdir -p .ssh
echo "gitlab.com,52.21.36.51 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> ~/.ssh/known_hosts
echo "gitlab.com,104.210.2.228 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> ~/.ssh/known_hosts
sudo sed --in-place /etc/apt/sources.list -e 's/archive.ubuntu.com/mirror.clarkson.edu/g'
sudo sed --in-place /etc/apt/sources.list -e 's/security.ubuntu.com/mirror.clarkson.edu/g'
sudo dpkg --add-architecture i386
sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y
sudo apt-get install -y python python-pip qemu-system libxml2 build-essential \
    git clang llvm gcc-arm-linux-gnueabi gcc-arm-linux-gnueabihf libc6-dev-i386 \
    gcc-4.8-multilib-arm-linux-gnueabihf gcc-4.8-arm-linux-gnueabihf cmake \
    libssl-dev
sudo pip install --upgrade tempita

sed -i '1i export RUST_TARGET_PATH=/vagrant/sel4-targets' ~/.bashrc
sed -i '1i export PATH=/home/ubuntu/.cargo/bin:$PATH' ~/.bashrc
source ~/.bashrc

pushd /vagrant >/dev/null
bash get_new_repos.sh
popd >/dev/null

curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly-2017-04-04

cargo install xargo
