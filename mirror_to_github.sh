#!/usr/bin/env bash

for repo in $(cat .subrepos); do
    pushd $repo
    git push --mirror ssh://git@github.com/robigalia/$repo.git
    popd
done
