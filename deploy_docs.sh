#!/bin/sh

mv upstream_docs/i686-sel4-robigalia/doc upstream_docs/i686
mv upstream_docs/x86_64-sel4-robigalia/doc upstream_docs/x86_64
mv upstream_docs/arm-sel4-robigalia/doc upstream_docs/arm

rm -rf upstream_docs/i686-sel4-robigalia
rm -rf upstream_docs/x86_64-sel4-robigalia
rm -rf upstream_docs/arm-sel4-robigalia

rm -rf upstream_docs/debug
rsync -e 'ssh -4' -raxv --delete upstream_docs/ cmr@octayn.net:doc.robigalia.org/
